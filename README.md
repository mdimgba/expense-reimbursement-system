**Expense Reimbursement System**

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

**Features:**

- Login as a General Employee or as an administrator.
 
- Personalized greeting for each user.
- (General) See all submitted reimbursements.
- (General) Filter reimbursements by resolved and unresolved.
- (General) Submit a new reimbursement.
- (General) Logout.
- (Admin) See all pending reimbursements for every employee at the company.
- (Admin) Resolve reimbursements with approved or denied.
- (Admin) See all reimbursements ever submitted and filter by pending, approved, or denied.


**To-Do List:**

- General Employees can submit pictures of their receipts, if applicable.
- Create a hiring director page that allows new employees to be added to the system (new employee receives an email with a temporary password).

**Technologies Used:**

**_Back-end_**

    Maven
    Java
    Sql
    Javalin
    Postman (endpoint testing)
    JDBC
    Mockito (testing)
    JUnit (testing)

**_Front-End_**

    HTML
    CSS
    Javascript
    Selenium (testing)

**Getting Started:**
1. Clone Repository using git clone.

2. Open the project in your a workspace of your choice.

3. Launch project in AppDriver

4. Access login page at http://localhost:9989/html/login.html

**Usage:**

Create a new user in the MainDriver (Your choice of employee, administrator, or both).
You will then be able to sign in using this information at the login page, and use the app to submit and/or resolve reimbursements.



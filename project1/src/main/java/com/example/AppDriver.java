package com.example;


import com.example.controller.RefundController;
import com.example.controller.RefundControllerAdmin;
import com.example.dao.RefundDBConnection;
import com.example.dao.RefundDao;
import com.example.service.RefundService;
import com.example.service.RefundServiceAdmin;

import io.javalin.Javalin;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

public class AppDriver {
	
	public final static Logger log = Logger.getLogger(AppDriver.class);

	public static void main(String[] args) {

		//RefundDao rDao = new RefundDao(new RefundDBConnection());
		//RefundService rServ = new RefundService(new RefundDao(new RefundDBConnection()));
		RefundController rCon = new RefundController(new RefundService(new RefundDao(new RefundDBConnection())));
		RefundControllerAdmin rConA = new RefundControllerAdmin(new RefundServiceAdmin(new RefundDao(new RefundDBConnection())));
		
		//potential route to go if I decide I want 1 controller
		//RefundController rConA = new RefundControllerAdmin(new RefundService(new RefundDao(new RefundDBConnection())));
		
		
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
			config.enableCorsForAllOrigins();
		});

		app.start(9989);
		
		app.post("/refund/login", rCon.postLogin);
		app.get("/employees/session", rCon.getSessRefund);
		
		app.get("/employees/all", rCon.getRetrieveReimbursementsForEmployee);
		app.get("/employees/pending", rCon.getViewPendingTickets);
		app.get("/employees/resolved", rCon.getViewPastTickets);
		
		app.post("/employees/new", rCon.postAddNewTicket);
		
		app.get("/admin/pending", rConA.getViewAllPendingReimbursements);
		app.get("/admin/approved", rConA.getViewAllApprovedReimbursements);
		app.get("/admin/denied", rConA.getViewAllDeniedReimbursements);
		
		// actually a put, but I can't submit form data that way
		app.post("/admin/decide", rConA.putDecideReimbursement);
		
		app.get("/employees/end", rCon.endSessRefund);
		
		// It's not sending to the failedlogin page with a failed attempt like I want it to
		// but general employees and admin go to their correct pages
		//Fixed
		app.exception(NullPointerException.class, (e, ctx)->{
			ctx.status(404);
			ctx.redirect("/html/failedlogin.html");
			ctx.result("User does not exist");
		});
		
	}
}

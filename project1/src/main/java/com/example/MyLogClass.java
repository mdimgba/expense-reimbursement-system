package com.example;

import com.example.controller.RefundController;

public class MyLogClass {
	
	public void method1Resolved() {
		RefundController.log.info("Reimbursement resolved");
	}
	
	public void method2Added() {
		RefundController.log.info("New Reimbursement added");
	}

}

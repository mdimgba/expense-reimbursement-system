package com.example;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.example.page.EmployeeLoginPage;

public class Selenium {
	public static void main(String[] args) {

		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

		WebDriver driver = new ChromeDriver();

//			driver.get("http://google.com");
//			
//			//This tells selenium to wait 2 seconds when finding an element before throwing an exception
//			driver.manage().timeouts().implicitlyWait(2,  TimeUnit.SECONDS);
//			
//			WebElement searchBar = driver.findElement(By.name("q"));
//			WebElement searchButton = driver.findElement(By.name("btnK"));
//			
//			searchBar.sendKeys("Do a barrel roll");
//			//searchBar.sendKeys(Keys.ENTER);
//			searchButton.click();
//			
//			//Why doesn't this work?
//			//TimeUnit.SECONDS.sleep(10);
//			
//			driver.get("http://google.com");
//			
//			driver.quit();

		EmployeeLoginPage page = new EmployeeLoginPage(driver);
		page.navigateTo();

	}
}

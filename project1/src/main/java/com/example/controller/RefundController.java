package com.example.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.service.RefundService;
import com.example.service.RefundServiceAdmin;

import io.javalin.http.Handler;


public class RefundController {
	
	public final static Logger log = Logger.getLogger(RefundController.class);
	// get/post/etc. endpoints go here
	private RefundService rServ;
	private RefundServiceAdmin rServA;
	
	public RefundController() {
		// TODO Auto-generated constructor stub
	}

	public RefundController(RefundService rServ) {
		super();
		this.rServ = rServ;
	}
	
	
	// POSTLOGIN
	public Handler postLogin = (ctx) ->{
		// call service methods in here that run my desired functionality
		// TEST DRIVEN DEVELOPMENT
		
		if(rServ.verifyLoginCredentials(ctx.formParam("username"), ctx.formParam("password"))) {
			//System.out.println();
			if(rServ.verifyAdmin(ctx.formParam("username"))) {
				//System.out.println("Admin login");
				ctx.sessionAttribute("emp", rServ.getEmployee(ctx.formParam("username")));
				ctx.redirect("/html/admin.html");
			}else {
				//System.out.println("General Employee login");
				ctx.sessionAttribute("emp", rServ.getEmployee(ctx.formParam("username")));
				ctx.redirect("/html/notadmin.html");
			}
		} else { //This one can't be accessed b/c a NullPointerException is thrown
			System.out.println("Failed login");
			ctx.redirect("/html/failedlogin.html");
		}
	};
		
	// GETSESSREFUND
	public Handler getSessRefund = (ctx)->{
		//System.out.println("In get session");
		//System.out.println((Employee) ctx.sessionAttribute("emp"));
		Employee emp = (Employee) ctx.sessionAttribute("emp");
		//System.out.println("Login: " + emp);
		
//		boolean admin = emp.getIsAdmin();
//		System.out.println(admin);
		ctx.json(emp);
//		ctx.sessionAttributeMap();		
	};
	
	// ENDSESSREFUND
	public Handler endSessRefund = (ctx)->{
		//Call when a logout button is pressed on either the admin or notadmin page
		
		//This works for logging out, but now the lag is killing my ability to send employee sessions
		ctx.sessionAttribute("emp", null);
		//System.out.println("Logout: " + (Employee) ctx.sessionAttribute("emp"));
	};
	
	// GETRETRIEVEREIMBURSEMENTSFOREMPLOYEE
	public Handler getRetrieveReimbursementsForEmployee = (ctx)->{
		// Button to press. Takes employee data from session
		Employee emp = ctx.sessionAttribute("emp");
		List<Reimbursement> rList = rServ.retrieveReimbursemtsForEmployee(emp);
		// Displays results on the screen. Sends the list as JSON that I can
		// display to the user in table format using javascript
		ctx.json(rList);
		
	};
	
	// GETVIEWPASTTICKETS
	public Handler getViewPastTickets = (ctx)->{
		// Button clicked. Employee Data from session. Nav bar to host all buttons
		Employee emp = (Employee) ctx.sessionAttribute("emp");
		
		List<Reimbursement> rList = rServ.viewPastTickets(emp);
		
		//Send to javascript using json
		ctx.json(rList);
		
	};
	
	// GETVIEWPENDINGTICKETS
	public Handler getViewPendingTickets = (ctx)->{
		Employee emp = (Employee) ctx.sessionAttribute("emp");
		
		List<Reimbursement> rList = rServ.viewPendingTickets(emp);
		
		ctx.json(rList);
	};
	
	// static final POSTADDNEWTICKET
	public Handler postAddNewTicket = (ctx)->{
		// add a new row to the pending tickets column by adding a row to 
		// the database.
		// Relay whether or not the interaction was successful.
		//Javascript alert?
		
		//System.out.println("In post new ticket");
		Double amount = Double.parseDouble(ctx.formParam("amount"));
		//System.out.println("Amount is: " + amount);
		String desc = ctx.formParam("description");
		//System.out.println("Description is: " + desc);
		Integer tId = Integer.parseInt(ctx.formParam("typeId"));
		//System.out.println("Type id is: " + tId);
		
		Employee author = (Employee) ctx.sessionAttribute("emp");
		int authorId = author.getUserId();
		
		// no receipt
		Reimbursement newReimb = new Reimbursement(amount, desc, authorId, tId);
		
		// with receipt
		//System.out.println(newReimb);
		rServ.addNewTicket(author, newReimb);
		
		log.setLevel((Level) Level.INFO);
		log.info("New Reimbursement added");
		//System.out.println("New ticket posted");
		ctx.redirect("/html/naold.html");
		
		
		//Something to let the user know if it works
	};
	
}

package com.example.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.models.ReimbursementStatus;
import com.example.service.RefundServiceAdmin;

import io.javalin.http.Handler;

public class RefundControllerAdmin {
	
	public final static Logger log = Logger.getLogger(RefundControllerAdmin.class);
	
	private RefundServiceAdmin rServA;
	
	public RefundControllerAdmin() {
		// TODO Auto-generated constructor stub
	}

	public RefundControllerAdmin(RefundServiceAdmin rServA) {
		super();
		this.rServA = rServA;
	}
	
	public Handler getViewAllPendingReimbursements = (ctx)->{
		
		System.out.println("In all pending");
		Employee admin = (Employee) ctx.sessionAttribute("emp");
		System.out.println(admin);
		
		List<Reimbursement> rList = rServA.viewAllPendingReimbursements(admin);
		
		ctx.json(rList);
	};
	
	public Handler getViewAllApprovedReimbursements = (ctx)->{

		System.out.println("In all approved");
		Employee admin = (Employee) ctx.sessionAttribute("emp");
		System.out.println(admin);
		
		List<Reimbursement> rList = rServA.getApprovedReimbursements(admin);
		System.out.println(rList);
		ctx.json(rList);
	};
	
	public Handler getViewAllDeniedReimbursements = (ctx)->{

		System.out.println("In all denied");
		Employee admin = (Employee) ctx.sessionAttribute("emp");
		System.out.println(admin);
		
		List<Reimbursement> rList = rServA.getDeniedReimbursements(admin);
		System.out.println(rList);
		ctx.json(rList);
	};
	
	public Handler putDecideReimbursement = (ctx)->{
		
		// Relay whether the decision was successful or not. Or simply show
		// an updated version of the table if it was successful, and relay
		// that it was not successful with a helpful message if it wasn't
		
		//System.out.println("In decide reimbursement");
		// JavaScript alert?
		Employee admin = (Employee) ctx.sessionAttribute("emp");
		//System.out.println(admin);
		
		Integer stat = Integer.parseInt(ctx.formParam("statusId"));
		ReimbursementStatus decision = new ReimbursementStatus(stat);
		//System.out.println(decision);
		
		Integer reimbId = Integer.parseInt(ctx.formParam("reimbursementId"));
		Reimbursement original = rServA.getReimbursementWithId(reimbId);
		//System.out.println(original);
		
		ctx.redirect("/html/adminpending.html");
		rServA.decideReimbursement(admin, original, decision);
		
		log.setLevel((Level) Level.INFO);
		log.info("Reimbursement resolved");
		//System.out.println("update successful");
		//Something to let the user know if it works
	};
	
	public Handler getFilterReimbursementByStatus = (ctx)->{
		Employee admin = (Employee) ctx.sessionAttribute("emp");
		
		// I will need to seek user input on the filter (using a radio button)
		Integer stat = Integer.parseInt(ctx.formParam("status"));
		ReimbursementStatus newStat = new ReimbursementStatus(stat);
		
		List<Reimbursement> rList = rServA.filterReimbursementsByStatus(admin, newStat);
		
		// Display results in a table
		ctx.json(rList);
	};

}

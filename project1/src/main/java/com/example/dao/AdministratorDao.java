package com.example.dao;

import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.models.ReimbursementStatus;

public interface AdministratorDao <E>{
	
	public E findAllEmployees();
	public E findEmployeeById(int id);
	// Might bring sub-optimal data, duplicate names
	public E findEmployeeByName(String fName, String lName);
	// Would necessitate mandatory usernames
	public E findEmployeeByUsername(String username);
	
	public E findAllReimbursements();
	public E findAllPendingReimbursements();
	// admin will also have access to findReimbursementsById
	
	// Method to find all people in the company with admin privileges
	public E findAllAdmins();
	
	// where the reimbursement status is 1(pending), allowed to update
	// to 2(approved) or 3(denied)
	// public void resolveRequest(ReimbursementStatus status);
	public void resolveRequest(ReimbursementStatus status, Reimbursement original, Employee admin);
	public Reimbursement findRequestById(int requestId);
}

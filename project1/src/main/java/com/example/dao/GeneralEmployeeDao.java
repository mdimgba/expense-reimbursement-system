package com.example.dao;

import com.example.models.Reimbursement;

public interface GeneralEmployeeDao <E>{
	
	// dao methods are more general CRUD stuff
	
	// return all reimbursements associated w/ an employee
	// parse pending from finalized in business logic
	public E findReimbursementsById(int id);
	
	// return reimbursement types (I think the model takes care of this, but whatever)
	public E findReimbursementTypes();
	
	// return potential reimbursement statuses
	public E findReimbursementStatuses();
	
	// I want to add a add reimbursement request slot here, but the
	// number of parameters it would need gives me pause. 
	// What is the criteria for a service layer method vs. a DAO layer object
	// I think dao is bare bones access and service layer builds it up
	// I can just insert a reimbursement object with a constructor adjusted to 
	// only accept necessary information
	public void addReimbursementRequest(Reimbursement request);
	
}

package com.example.dao;

import com.example.models.Employee;

public interface OverseerDao <E>{

	// I want this functionality for my myself. It isn't necessarily
		// an admin privilege, but a general employee won't be able to add
		// employees either
		public void insertEmployee(Employee emp);
		
}

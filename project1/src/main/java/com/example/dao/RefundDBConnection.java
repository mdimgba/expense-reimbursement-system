package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class RefundDBConnection {
	private String url = "jdbc:mariadb://database-1.cxmyzlwu53kp.us-east-2.rds.amazonaws.com:3306/refunddb";
	private String username = "refunduser";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
}

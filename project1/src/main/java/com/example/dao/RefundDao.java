package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.models.ReimbursementStatus;
import com.example.models.ReimbursementType;

public class RefundDao implements GeneralEmployeeDao, AdministratorDao, OverseerDao {

	private RefundDBConnection rdc;

	public RefundDao() {
		// TODO Auto-generated constructor stub
	}

	public RefundDao(RefundDBConnection rdc) {
		super();
		this.rdc = rdc;
	}
	

	// This is a tough one. I'm not sure which constructor I will get, so i'm not
	// able to
	// tell which values I can write in yet
	// I can make it easy on myself for now and assume you need all the things i've listed
	// to make an account
	@Override
	public void insertEmployee(Employee emp) {
		// TODO Auto-generated method stub
		try (Connection con = rdc.getDBConnection()) {

			String sql = "INSERT INTO ers_users(ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) VALUES (?, ?, ?, ?, ?, ?)";

			// No idea how setting admin works b/c I have to convert from bool
			// to number from here to my db
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, emp.getUserName());
			ps.setString(2, emp.getPassword());
			ps.setString(3, emp.getFirstName());
			ps.setString(4, emp.getLastName());
			ps.setString(5, emp.getEmail());
			ps.setBoolean(6, emp.getIsAdmin());
			
			int check = ps.executeUpdate();
			
			if(check != 0) {
				System.out.println("Employee successfully added");
			} else {
				System.out.println("Employee add unsuccessful");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// @Override
	public List<Employee> findAllEmployees() {
		// TODO Auto-generated method stub
		List<Employee> eList = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_users";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				eList.add(new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getBoolean(7)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eList;
	}

	@Override
	public Employee findEmployeeById(int id) {
		// TODO Auto-generated method stub
		Employee emp = null;
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_users WHERE ers_users_id=?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return emp;
			}
			emp = new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
					rs.getString(6), rs.getBoolean(7));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	// Nothing stopping multiple employees w/ the same name
	@Override
	public List<Employee> findEmployeeByName(String fName, String lName) {
		// TODO Auto-generated method stub
		List<Employee> eList = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_users WHERE user_first_name=? AND user_last_name=?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, fName);
			ps.setString(2, lName);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				eList.add(new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getBoolean(7)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eList;
	}

	@Override
	public Employee findEmployeeByUsername(String username) {
		// TODO Auto-generated method stub
		Employee emp = null;
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_users WHERE ers_username=?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return emp;
			}
			emp = new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
					rs.getString(6), rs.getBoolean(7));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public List<Reimbursement> findAllReimbursements() {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_reimbursement";
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}
	
	// I can turn this into find all reimbursements by shifting
	// to a ps with SELECT * FROM ers_reimbursement
	@Override
	public List<Reimbursement> findAllPendingReimbursements() {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			String sql = "{CALL find_pending_reimbursements() }";
			
			CallableStatement cs = con.prepareCall(sql);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}

	@Override
	public List<Employee> findAllAdmins() {
		// TODO Auto-generated method stub
		List<Employee> eList = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			// Try a callable return all employees where role=1, keep that info on the database side
			String sql = "{CALL find_all_admins() }";
			
			CallableStatement cs = con.prepareCall(sql);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				eList.add(new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
					rs.getString(6), rs.getBoolean(7)));
			}
		

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eList;
	}

	// Service implement - Selenium test
	@Override
	public void resolveRequest(ReimbursementStatus status, Reimbursement original, Employee admin) {
		// TODO Auto-generated method stub
		try (Connection con = rdc.getDBConnection()) {
			// call my admin update procedure
			// control access to this method with business logic in the service layer
			// should this method be private?
			String sql = "CALL admin_update(?, ?, ?, ?)";
			
			// Definitely need some service method to send in this data
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, status.getStatusId());
			cs.setInt(2, admin.getUserId());
			cs.setInt(3, original.getReimbId());
			cs.setInt(4, original.getReimbAuthor());
			
			int check = cs.executeUpdate();
			if(check != 0) {
				System.out.println("Reimbursement request successfully resolved");
			} else {
				System.out.println("Request resolve unsuccessful");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Reimbursement> findReimbursementsById(int id) {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_author=?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}

	// Service Implement
	// This one, and the next, might benefit from some business logic to display it
	// to the employee
	@Override
	public List<ReimbursementType> findReimbursementTypes() {
		// TODO Auto-generated method stub
		List<ReimbursementType> types = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_reimbursement_type";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				types.add(new ReimbursementType(rs.getInt(1), rs.getString(2)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return types;
	}

	@Override
	public List<ReimbursementStatus> findReimbursementStatuses() {
		// TODO Auto-generated method stub
		List<ReimbursementStatus> statuses = new ArrayList<>();
		try (Connection con = rdc.getDBConnection()) {

			String sql = "SELECT * FROM ers_reimbursement_status";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				statuses.add(new ReimbursementStatus(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statuses;
	}

	// Service Implement - Selenium test
	//Finish Me
	// Return types have no issues with the E generic in the dao, but
	// parameter types throw a "must override or implement a supertype method"
	// error.
	// Maybe it has to do w/ specificity? I can return what I like,
	// but if I place issues on what I accept then im doing injustice to the
	// generic
	@Override
	public void addReimbursementRequest(Reimbursement request) {
		// TODO Auto-generated method stub
		try (Connection con = rdc.getDBConnection()) {
			String sql = "INSERT INTO ers_reimbursement(reimb_amount, reimb_description, reimb_receipt, reimb_author, reimb_type_id) VALUES(?, ?, ?, ?, ?)";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setDouble(1, request.getReimbAmount());
			ps.setString(2, request.getDescription());
			ps.setBlob(3, request.getReceipt());
			ps.setInt(4, request.getReimbAuthor());
			ps.setInt(5, request.getReimbTypeId());
			
			int check = ps.executeUpdate();
			
			if(check != 0) {
				System.out.println("Reimbursement request successfully added");
			} else {
				System.out.println("Reimbursement add unsuccessful");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Reimbursement findRequestById(int requestId) {
		// TODO Auto-generated method stub
		Reimbursement request = null;
		try (Connection con = rdc.getDBConnection()) {
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_id=?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, requestId);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return request;
			}
			request = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
					rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return request;
	}


}

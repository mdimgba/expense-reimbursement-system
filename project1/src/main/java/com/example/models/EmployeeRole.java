package com.example.models;

public class EmployeeRole {
	
	int userRoleId;
	String userRole;
	
	public EmployeeRole() {
		// TODO Auto-generated constructor stub
	}

	public EmployeeRole(int userRoleId) {
		super();
		this.userRoleId = userRoleId;
	}

	public EmployeeRole(String userRole) {
		super();
		this.userRole = userRole;
	}

	public EmployeeRole(int userRoleId, String userRole) {
		super();
		this.userRoleId = userRoleId;
		this.userRole = userRole;
	}

	// All getters no setters. I don't want employees to be able to change
	// themselves into administrators. If their role in the company changes
	// they will just have to get a new id. Or maybe someone with db access
	// will be able to change it on that end
	public int getUserRoleId() {
		return userRoleId;
	}

	public String getUserRole() {
		return userRole;
	}

	@Override
	public String toString() {
		return "EmployeeRole [userRoleId=" + userRoleId + ", userRole=" + userRole + "]";
	}
	
	

}

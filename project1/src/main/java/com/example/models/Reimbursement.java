package com.example.models;

import java.sql.Blob;
import java.sql.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Reimbursement {
	
	private int reimbId;
	private double reimbAmount;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp subTime;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp resolvedTime;
	private String description;
	private Blob receipt;
	private int reimbAuthor;
	private int reimbResolver;
	private int reimbStatusId;
	private int reimbTypeId;
	
	public Reimbursement() {
		// TODO Auto-generated constructor stub
	}
		
	public Reimbursement(double reimbAmount, String description, int reimbAuthor, int reimbTypeId) {
		super();
		this.reimbAmount = reimbAmount;
		this.description = description;
		this.reimbAuthor = reimbAuthor;
		this.reimbTypeId = reimbTypeId;
	}

	public Reimbursement(double reimbAmount, String description, Blob receipt, int reimbAuthor, int reimbTypeId) {
		super();
		this.reimbAmount = reimbAmount;
		this.description = description;
		this.receipt = receipt;
		this.reimbAuthor = reimbAuthor;
		this.reimbTypeId = reimbTypeId;
	}
	
	// Used in mock testing everything above + reimbStatusId
	public Reimbursement(double reimbAmount, String description, int reimbAuthor, int reimbStatusId, int reimbTypeId) {
		super();
		this.reimbAmount = reimbAmount;
		this.description = description;
		this.reimbAuthor = reimbAuthor;
		this.reimbStatusId = reimbStatusId;
		this.reimbTypeId = reimbTypeId;
	}
	
	// Everything except receipt
	public Reimbursement(int reimbId, double reimbAmount, Timestamp subTime, Timestamp resolvedTime, String description,
			int reimbAuthor, int reimbResolver, int reimbStatusId, int reimbTypeId) {
		super();
		this.reimbId = reimbId;
		this.reimbAmount = reimbAmount;
		this.subTime = subTime;
		this.resolvedTime = resolvedTime;
		this.description = description;
		this.reimbAuthor = reimbAuthor;
		this.reimbResolver = reimbResolver;
		this.reimbStatusId = reimbStatusId;
		this.reimbTypeId = reimbTypeId;
	}


	public Reimbursement(int reimbId, double reimbAmount, Timestamp subTime, Timestamp resolvedTime, String description,
			Blob receipt, int reimbAuthor, int reimbResolver, int reimbStatusId, int reimbTypeId) {
		super();
		this.reimbId = reimbId;
		this.reimbAmount = reimbAmount;
		this.subTime = subTime;
		this.resolvedTime = resolvedTime;
		this.description = description;
		this.receipt = receipt;
		this.reimbAuthor = reimbAuthor;
		this.reimbResolver = reimbResolver;
		this.reimbStatusId = reimbStatusId;
		this.reimbTypeId = reimbTypeId;
	}

	public int getReimbId() {
		return reimbId;
	}

	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}

	public double getReimbAmount() {
		return reimbAmount;
	}

	public void setReimbAmount(double reimbAmount) {
		this.reimbAmount = reimbAmount;
	}

	public Timestamp getSubTime() {
		return subTime;
	}

	public void setSubTime(Timestamp subTime) {
		this.subTime = subTime;
	}

	public Timestamp getResolvedTime() {
		return resolvedTime;
	}

	public void setResolvedTime(Timestamp resolvedTime) {
		this.resolvedTime = resolvedTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Blob getReceipt() {
		return receipt;
	}
	
	public void setReceipt(Blob receipt) {
		this.receipt = receipt;
	}
	
	public int getReimbAuthor() {
		return reimbAuthor;
	}

	public void setReimbAuthor(int reimbAuthor) {
		this.reimbAuthor = reimbAuthor;
	}

	public int getReimbResolver() {
		return reimbResolver;
	}

	public void setReimbResolver(int reimbResolver) {
		this.reimbResolver = reimbResolver;
	}

	public int getReimbStatusId() {
		return reimbStatusId;
	}

	public void setReimbStatusId(int reimbStatusId) {
		this.reimbStatusId = reimbStatusId;
	}

	public int getReimbTypeId() {
		return reimbTypeId;
	}

	public void setReimbTypeId(int reimbTypeId) {
		this.reimbTypeId = reimbTypeId;
	}

	@Override
	public String toString() {
		return "Reimbursement [reimbId=" + reimbId + ", reimbAmount=" + reimbAmount + ", subTime=" + subTime
				+ ", resolvedTime=" + resolvedTime + ", description=" + description + ", reimbAuthor=" + reimbAuthor
				+ ", reimbResolver=" + reimbResolver + ", reimbStatusId=" + reimbStatusId + ", reimbTypeId="
				+ reimbTypeId + "]";
	}
	
	
	
	
}

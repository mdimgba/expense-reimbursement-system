package com.example.models;

public class ReimbursementStatus {

	int statusId;
	String status;
	
	
	public ReimbursementStatus() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementStatus(int statusId) {
		super();
		this.statusId = statusId;
	}


	public ReimbursementStatus(String status) {
		super();
		this.status = status;
	}

	public ReimbursementStatus(int statusId, String status) {
		super();
		this.statusId = statusId;
		this.status = status;
	}

	//same thing with all getters. The status can be changed by an
	//administrator, but that will send a command directly to the db.
	//The java end can only read back what the db says
	
	public int getStatusId() {
		return statusId;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "ReimbursementStatus [statusId=" + statusId + ", status=" + status + "]";
	}
	
}

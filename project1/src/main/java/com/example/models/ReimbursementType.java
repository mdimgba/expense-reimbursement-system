package com.example.models;

public class ReimbursementType {

	int typeId;
	String type;
	
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementType(int typeId) {
		super();
		this.typeId = typeId;
	}

	public ReimbursementType(String type) {
		super();
		this.type = type;
	}

	public ReimbursementType(int typeId, String type) {
		super();
		this.typeId = typeId;
		this.type = type;
	}


	public int getTypeId() {
		return typeId;
	}


	public String getType() {
		return type;
	}


	@Override
	public String toString() {
		return "ReimbursementType [typeId=" + typeId + ", type=" + type + "]";
	}
	
}

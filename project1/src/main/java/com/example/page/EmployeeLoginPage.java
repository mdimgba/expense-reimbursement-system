package com.example.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EmployeeLoginPage {

	private WebDriver driver;
	private WebElement header;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement submitButton;
	
	public EmployeeLoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.header = driver.findElement(By.tagName("h1"));
		this.usernameField = driver.findElement(By.name("username"));
		this.passwordField = driver.findElement(By.name("password"));
		this.submitButton = driver.findElement(By.name("employeesubmit"));
	}
	public void setUserName(String name) {
		this.usernameField.clear();
		this.usernameField.sendKeys(name);
	}
	
	public String getUserName() {
		return this.usernameField.getAttribute("value");
	}
	
	public void setPassword(String password) {
		this.passwordField.clear();
		this.passwordField.sendKeys(password);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9989/html/login.html");
	}
	
}

package com.example.service;

import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.models.ReimbursementStatus;

public interface AdministratorPriv <E>{

	// Admin Id will be the session id in this case
	// I have to tie up access to these functions with admin access somehow
	
	public E viewAllPendingReimbursements(Employee adminId);
	
	//decides whether to approve or deny. Prompts for the
	//word "approve" or "deny" and turns it into a code for 
	//the database (only accepts approve(2) or deny(3)
	public void decideReimbursement(Employee adminId, Reimbursement original, ReimbursementStatus decision);
	
	// Filters the reimbursements by status
	public E filterReimbursementsByStatus(Employee adminId, ReimbursementStatus status);
	
	public E getReimbursementWithId(int reimbId);
}

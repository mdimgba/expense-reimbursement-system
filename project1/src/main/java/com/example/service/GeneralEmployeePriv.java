package com.example.service;

import com.example.models.Employee;
import com.example.models.Reimbursement;

public interface GeneralEmployeePriv <E>{

	// service layer methods
	
	// Get the employee objects from the session, when the user logs in
	
	// return all tickets with typeId corresponding to approved or denied
	// and associate each ticket with its outcome
	public E viewPastTickets(Employee emp);
	// allow an employee to write a new request
	//public E addRequest(int id, int amount);
	// return all tickets with the typeId corresponding to pending
	public E viewPendingTickets(Employee emp);
	
	public void addNewTicket(Employee emp, Reimbursement request);
	
}

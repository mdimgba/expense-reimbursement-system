package com.example.service;

import java.util.ArrayList;
import java.util.List;

import com.example.dao.RefundDao;
import com.example.models.Employee;
import com.example.models.Reimbursement;

public class RefundService implements GeneralEmployeePriv{
	// business logic goes here
	private RefundDao rDao;
	
	public RefundService() {
		// TODO Auto-generated constructor stub
	}

	public RefundService(RefundDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	// Next 3 methods used to verify login and send information related to the user
	// Tested
	public boolean verifyLoginCredentials(String username, String password) {
		Employee emp = rDao.findEmployeeByUsername(username);
		boolean exists = false;
		if(emp == null) {
			throw new NullPointerException();
		}
		else {
			if(emp.getPassword().equals(password)) {
				exists = true;
				// start session with emp
			}
		}
		return exists;
	}
	public boolean verifyAdmin(String username) {
		Employee emp = rDao.findEmployeeByUsername(username);
		boolean isAdmin = emp.getIsAdmin();
		return isAdmin;
	}
	
	public Employee getEmployee(String username) {
		Employee emp = rDao.findEmployeeByUsername(username);
		if(emp == null) {
			throw new NullPointerException();
		}
		return emp;
	}
	
	// Tested
	public List<Reimbursement> retrieveReimbursemtsForEmployee(Employee emp) {
		List<Reimbursement> rList = new ArrayList<>();
		rList = rDao.findReimbursementsById(emp.getUserId());
	
		if(rList.isEmpty()) {
			// throw custom exception that returns whether the
			// employee does not exist or if they just don't have
			// any reimbursements submitted
			throw new NullPointerException();
		}
		return rList;
	}
	
	
	/*
	// When a user logs in, I should have access to their id
	// which I can send to the db to handle prompts
	// Same thing for request id. It should be tied to the object somehow
	// I have to display it all on the screen
	*/
	
	//Tested
	@Override
	public List<Reimbursement> viewPastTickets(Employee emp) {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = retrieveReimbursemtsForEmployee(emp);
		List<Reimbursement> newList = new ArrayList<>();
		
		for(int i=0; i < rList.size(); i++) {
			if(rList.get(i).getReimbStatusId() == 2 || rList.get(i).getReimbStatusId() == 3) {
				newList.add(rList.get(i));
			}
		}
		
		if(newList.isEmpty()) {
			throw new NullPointerException();
		}
		return newList;
	}

	//Tested
	@Override
	public List<Reimbursement> viewPendingTickets(Employee emp) {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = retrieveReimbursemtsForEmployee(emp);
		List<Reimbursement> newList = new ArrayList<>();
		
		for(int i=0; i < rList.size(); i++) {
			if(rList.get(i).getReimbStatusId() == 1) {
				newList.add(rList.get(i));
			}
		}
		
		if(newList.isEmpty()) {
			throw new NullPointerException();
		}
		return newList;
	}
	
	// Another one where the dao doen't inherently return anything
	// Do I just change my voids to bool or int in all case?
	@Override
	public void addNewTicket(Employee emp, Reimbursement request) {
		// TODO Auto-generated method stub
		boolean created = false;
		rDao.addReimbursementRequest(request);
		
		// return created;
	}
	
	

	
	
}

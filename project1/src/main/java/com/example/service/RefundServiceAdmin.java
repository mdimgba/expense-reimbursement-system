package com.example.service;

import java.util.ArrayList;
import java.util.List;

import com.example.dao.RefundDao;
import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.models.ReimbursementStatus;

public class RefundServiceAdmin implements AdministratorPriv {
	// business logic goes here
	private RefundDao rDao;
	private RefundService rServ;

	public RefundServiceAdmin() {
		// TODO Auto-generated constructor stub
	}

	public RefundServiceAdmin(RefundDao rDao) {
		super();
		this.rDao = rDao;
	}

	/*
	// Tested- Redundant
	public boolean verifyLoginCredentials(String username, String password) {
		Employee emp = rDao.findEmployeeByUsername(username);
		boolean exists = false;
		if (emp == null) {
			throw new NullPointerException();
		} else {
			if (emp.getPassword().equals(password)) {
				exists = true;
				// Start Session with emp
			}
		}
		return exists;
	}
	*/

	@Override
	public List<Reimbursement> viewAllPendingReimbursements(Employee admin) {
		// TODO Auto-generated method stub
		if (admin.getIsAdmin()) {
			List<Reimbursement> rList = rDao.findAllPendingReimbursements();
			return rList;
		}
		// Custom Exception that states user is not registered as an admin
		else {
			throw new NullPointerException();
			//return null;
		}
	}

	// Definitely need to verify whether or not this is successful
	@Override
	public void decideReimbursement(Employee admin, Reimbursement original, ReimbursementStatus decision) {
		// TODO Auto-generated method stub
		if (admin.getIsAdmin()) {
			rDao.resolveRequest(decision, original, admin);
		}
		// Custom Exception that states user is not registered as an admin
		else {
			// return null;
		}
	}

	// Should this actually be a more general case of findAllPendingReimbursements?
	// And findAllPendingReimbursements just calls this for the case where status=1?
	@Override
	public List<Reimbursement> filterReimbursementsByStatus(Employee admin, ReimbursementStatus status) {
		// TODO Auto-generated method stub
		if (admin.getIsAdmin()) {
			// List<Reimbursement> rList = rServ.retrieveReimbursemtsForEmployee(empId);
			List<Reimbursement> rList = rDao.findAllReimbursements();
			List<Reimbursement> newList = new ArrayList<>();

			for (int i = 0; i < rList.size(); i++) {
				if (rList.get(i).getReimbStatusId() == status.getStatusId()) {
					newList.add(rList.get(i));
				}
			}
			return newList;
		}
		// Custom Exception that states user is not registered as an admin
		else {
			throw new NullPointerException();
			//return null;
		}
	}
	
	public List<Reimbursement> getApprovedReimbursements(Employee admin) {
		
		ReimbursementStatus good = new ReimbursementStatus(2);
		List<Reimbursement> rList = filterReimbursementsByStatus(admin, good);
		return rList;
	}
	
	public List<Reimbursement> getDeniedReimbursements(Employee admin) {
		
		ReimbursementStatus bad = new ReimbursementStatus(3);
		List<Reimbursement> rList = filterReimbursementsByStatus(admin, bad);
		return rList;
	}

	@Override
	public Reimbursement getReimbursementWithId(int reimbId) {
		// TODO Auto-generated method stub
		Reimbursement reimb = rDao.findRequestById(reimbId);
		return reimb;
	}

}

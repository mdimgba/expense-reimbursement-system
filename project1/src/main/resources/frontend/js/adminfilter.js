/**
 * JS filter for admin
 */

window.onload = function(){
 	//console.log("js is really linked");
 	
 	let emp2 = localStorage.userName;
 	//console.log(emp2);
 	//window.sessionstorage;
 	
	//getAllReimbursements();
	document.getElementById('pending').addEventListener("click", getPendingReimbursements);
	document.getElementById('approved').addEventListener("click", getApprovedReimbursements);
	document.getElementById('denied').addEventListener("click", getDeniedReimbursements);
 	
	document.getElementById('logout').addEventListener("click", getLogout);
 }
 
 
 function getPendingReimbursements(){
 	//location.reload();
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			let rList = JSON.parse(xhttp.responseText);
 			
 			//Delete all rows after 0
 			var dTable = document.getElementById("reimbTable");
 			var len = dTable.rows.length;
 			for(var l=1;l<len-1;l++){
 				dTable.deleteRow(l);
 			}
 			
 			
 			//Creating the table
 			var recordCount = 1;
 			var rowNum = 1;
 			var table = document.getElementById("reimbTable");
 			
 			//for(i in table){
 			//	console.log(i)
 			//}
 			
 			for(var i of rList){
 			var row = table.insertRow(rowNum);
 				//console.log("row #: " + recordCount);
 				var cell0 = row.insertCell(0);
 				cell0.innerHTML = recordCount
	 			
	 			//console.log(i.reimbId);
	 			var cell1 = row.insertCell(1);
	 			cell1.innerHTML = i.reimbId;
	 			
	 			//console.log(i.reimbAmount);
	 			var cell2 = row.insertCell(2);
	 			cell2.innerHTML = i.reimbAmount;
	 			
	 			//console.log(i.subTime);
	 			var cell3 = row.insertCell(3);
	 			cell3.innerHTML = i.subTime;
	 			
	 			//console.log(i.resolvedTime);
	 			var cell4 = row.insertCell(4);
	 			cell4.innerHTML = i.resolvedTime;
	 			
	 			//console.log(i.description);
	 			var cell5 = row.insertCell(5);
	 			cell5.innerHTML = i.description;
	 			
	 			//console.log(i.receipt);
	 			var cell6 = row.insertCell(6);
	 			cell6.innerHTML = i.receipt;
			
				//console.log(i.reimbAuthor);
	 			var cell7 = row.insertCell(7);
	 			cell7.innerHTML = i.reimbAuthor;
	 				 			
	 			//console.log(i.reimbResolver);
	 			var cell8 = row.insertCell(8);
	 			cell8.innerHTML = i.reimbResolver;
	 			
	 			//// 1-Pending, 2-Approved, 3-Denied
	 			var status = i.reimbStatusId;
	 			if(status == 1){
	 				status = "Pending";
	 			}
	 			if(status == 2){
	 				status = "Approved";
	 			}
	 			if(status == 3){
	 				status = "Denied";
	 			}
	 			//console.log(i.reimbStatusId);
	 			var cell9 = row.insertCell(9);
	 			cell9.innerHTML = status;
	 			
	 			//// (1-Lodging, 2-Food, 3-Travel, 4-Other)
	 			var type = i.reimbTypeId;
	 			if(type == 1){
	 				type = "Lodging";
	 			}
	 			if(type == 2){
	 				type = "Food";
	 			}
	 			if(type == 3){
	 				type = "Travel";
	 			}
	 			if(type == 4){
	 				type = "Other";
	 			}
	 			//console.log(i.reimbTypeId);
	 			var cell10 = row.insertCell(10);
	 			cell10.innerHTML = type;
 			recordCount++;
 			rowNum++
 			}
 			//console.log(table.rows.length);
 			
 		}
 			
 	}
 	
 	xhttp.open("GET", "http://localhost:9989/admin/pending");
 	
 	xhttp.send();
 
 }

 function getApprovedReimbursements(){
 	//location.reload();
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			let rList = JSON.parse(xhttp.responseText);
			//console.log(rList);
			
 			//Delete all rows after 0
 			var dTable = document.getElementById("reimbTable");
 			var len = dTable.rows.length;
 			for(var l=1;l<len-1;l++){
 				dTable.deleteRow(l);
 			}
 			
 			
 			//Creating the table
 			var recordCount = 1;
 			var rowNum = 1;
 			var table = document.getElementById("reimbTable");
 						
 			//for(i in table){
 			//	console.log(i)
 			//}
 			
 			for(var i of rList){
 			var row = table.insertRow(rowNum);
 				//console.log("row #: " + recordCount);
 				var cell0 = row.insertCell(0);
 				cell0.innerHTML = recordCount
	 			
	 			//console.log(i.reimbId);
	 			var cell1 = row.insertCell(1);
	 			cell1.innerHTML = i.reimbId;
	 			
	 			//console.log(i.reimbAmount);
	 			var cell2 = row.insertCell(2);
	 			cell2.innerHTML = i.reimbAmount;
	 			
	 			//console.log(i.subTime);
	 			var cell3 = row.insertCell(3);
	 			cell3.innerHTML = i.subTime;
	 			
	 			//console.log(i.resolvedTime);
	 			var cell4 = row.insertCell(4);
	 			cell4.innerHTML = i.resolvedTime;
	 			
	 			//console.log(i.description);
	 			var cell5 = row.insertCell(5);
	 			cell5.innerHTML = i.description;
	 			
	 			//console.log(i.receipt);
	 			var cell6 = row.insertCell(6);
	 			cell6.innerHTML = i.receipt;
			
				//console.log(i.reimbAuthor);
	 			var cell7 = row.insertCell(7);
	 			cell7.innerHTML = i.reimbAuthor;
	 				 			
	 			//console.log(i.reimbResolver);
	 			var cell8 = row.insertCell(8);
	 			cell8.innerHTML = i.reimbResolver;
	 			
	 			//// 1-Pending, 2-Approved, 3-Denied
	 			var status = i.reimbStatusId;
	 			if(status == 1){
	 				status = "Pending";
	 			}
	 			if(status == 2){
	 				status = "Approved";
	 			}
	 			if(status == 3){
	 				status = "Denied";
	 			}
	 			//console.log(i.reimbStatusId);
	 			var cell9 = row.insertCell(9);
	 			cell9.innerHTML = status;
	 			
	 			//// (1-Lodging, 2-Food, 3-Travel, 4-Other)
	 			var type = i.reimbTypeId;
	 			if(type == 1){
	 				type = "Lodging";
	 			}
	 			if(type == 2){
	 				type = "Food";
	 			}
	 			if(type == 3){
	 				type = "Travel";
	 			}
	 			if(type == 4){
	 				type = "Other";
	 			}
	 			//console.log(i.reimbTypeId);
	 			var cell10 = row.insertCell(10);
	 			cell10.innerHTML = type;
 			recordCount++;
 			rowNum++
 			}
 			//console.log(table.rows.length);
 		}
 	}
 	
 	xhttp.open("GET", "http://localhost:9989/admin/approved");
 	
 	xhttp.send();
 
 }

 function getDeniedReimbursements(){
 	//location.reload();
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
			let rList = JSON.parse(xhttp.responseText);
			//console.log(rList);	
			//for(var i in rList){
			//console.log(i);
			//}
			
			
			//Delete all rows after 0
 			var dTable = document.getElementById("reimbTable");
 			var len = dTable.rows.length;
 			for(var l=1;l<len-1;l++){
 				dTable.deleteRow(l);
 			}
 			
 			
 			//Creating the table
 			var recordCount = 1;
 			var rowNum = 1;
 			var table = document.getElementById("reimbTable");
 			
 			//for(i in table){
 			//	console.log(i)
 			//}
 			
 			for(var i of rList){
 			var row = table.insertRow(rowNum);
 				//console.log("row #: " + recordCount);
 				var cell0 = row.insertCell(0);
 				cell0.innerHTML = recordCount
	 			
	 			//console.log(i.reimbId);
	 			var cell1 = row.insertCell(1);
	 			cell1.innerHTML = i.reimbId;
	 			
	 			//console.log(i.reimbAmount);
	 			var cell2 = row.insertCell(2);
	 			cell2.innerHTML = i.reimbAmount;
	 			
	 			//console.log(i.subTime);
	 			var cell3 = row.insertCell(3);
	 			cell3.innerHTML = i.subTime;
	 			
	 			//console.log(i.resolvedTime);
	 			var cell4 = row.insertCell(4);
	 			cell4.innerHTML = i.resolvedTime;
	 			
	 			//console.log(i.description);
	 			var cell5 = row.insertCell(5);
	 			cell5.innerHTML = i.description;
	 			
	 			//console.log(i.receipt);
	 			var cell6 = row.insertCell(6);
	 			cell6.innerHTML = i.receipt;
			
				//console.log(i.reimbAuthor);
	 			var cell7 = row.insertCell(7);
	 			cell7.innerHTML = i.reimbAuthor;
	 				 			
	 			//console.log(i.reimbResolver);
	 			var cell8 = row.insertCell(8);
	 			cell8.innerHTML = i.reimbResolver;
	 			
	 			//// 1-Pending, 2-Approved, 3-Denied
	 			var status = i.reimbStatusId;
	 			if(status == 1){
	 				status = "Pending";
	 			}
	 			if(status == 2){
	 				status = "Approved";
	 			}
	 			if(status == 3){
	 				status = "Denied";
	 			}
	 			//console.log(i.reimbStatusId);
	 			var cell9 = row.insertCell(9);
	 			cell9.innerHTML = status;
	 			
	 			//// (1-Lodging, 2-Food, 3-Travel, 4-Other)
	 			var type = i.reimbTypeId;
	 			if(type == 1){
	 				type = "Lodging";
	 			}
	 			if(type == 2){
	 				type = "Food";
	 			}
	 			if(type == 3){
	 				type = "Travel";
	 			}
	 			if(type == 4){
	 				type = "Other";
	 			}
	 			//console.log(i.reimbTypeId);
	 			var cell10 = row.insertCell(10);
	 			cell10.innerHTML = type;
 			recordCount++;
 			rowNum++
 			}
 			//console.log(table.rows.length);
 			
 		}
 	}
 	
 	xhttp.open("GET", "http://localhost:9989/admin/denied");
 	
 	xhttp.send();
 
 }

 
  function getLogout(){
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			logout();
 		}
 	}
 	
 	//xhttp.open("GET", "http://localhost:9989/employees/session");
 	
 	xhttp.send();
 
 }
 
 
 function logout(){
 	//console.log("In logout function");
 	localStorage.clear();
 }
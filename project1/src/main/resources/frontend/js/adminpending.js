/**
 *  Very similar to employee old js file
 */
 
 window.onload = function(){
 	console.log("js is really linked");
 	
 	let emp2 = localStorage.userName;
 	//console.log(emp2);
 	//window.sessionstorage;
 	
	//getAllReimbursements();
	getAllPendingReimbursements();
 	
	document.getElementById('logout').addEventListener("click", getLogout);
 }
 
 function getAllPendingReimbursements(){
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			let rList = JSON.parse(xhttp.responseText);
			//console.log(rList);
			
 			//Delete all rows after 0
 			var dTable = document.getElementById("reimbTable");
 			var len = dTable.rows.length;
 			for(var l=1;l<len-1;l++){
 				dTable.deleteRow(l);
 			}
 			
 			//Creating the table
 			var recordCount = 1;
 			var rowNum = 1;
 			var table = document.getElementById("reimbTable");
 						
 			//for(i in table){
 			//	console.log(i)
 			//}
 			
 			for(var i of rList){
 			var row = table.insertRow(rowNum);
 				//console.log("row #: " + recordCount);
 				var cell0 = row.insertCell(0);
 				cell0.innerHTML = recordCount
	 			
	 			//console.log(i.reimbId);
	 			var cell1 = row.insertCell(1);
	 			cell1.innerHTML = i.reimbId;
	 			
	 			//console.log(i.reimbAmount);
	 			var cell2 = row.insertCell(2);
	 			cell2.innerHTML = i.reimbAmount;
	 			
	 			//console.log(i.subTime);
	 			var cell3 = row.insertCell(3);
	 			cell3.innerHTML = i.subTime;
	 				 			
	 			//console.log(i.description);
	 			var cell4 = row.insertCell(4);
	 			cell4.innerHTML = i.description;
	 			
	 			//console.log(i.receipt);
	 			var cell5 = row.insertCell(5);
	 			cell5.innerHTML = i.receipt;
	 				 			
	 			//console.log(i.reimbAuthor);
	 			var cell6 = row.insertCell(6);
	 			cell6.innerHTML = i.reimbAuthor;
	 			 			
	 			//// (1-Lodging, 2-Food, 3-Travel, 4-Other)
	 			var type = i.reimbTypeId;
	 			if(type == 1){
	 				type = "Lodging";
	 			}
	 			if(type == 2){
	 				type = "Food";
	 			}
	 			if(type == 3){
	 				type = "Travel";
	 			}
	 			if(type == 4){
	 				type = "Other";
	 			}
	 			//console.log(i.reimbTypeId);
	 			var cell7 = row.insertCell(7);
	 			cell7.innerHTML = type;
	 			
	 			//var cell8 = row.insertCell(8);
	 			//cell8.innerHTML = "resolve";
	 			
 			recordCount++;
 			rowNum++
 			}
 			//console.log(table.rows.length);
 		}
 	}
 	
 	xhttp.open("GET", "http://localhost:9989/admin/pending");
 	
 	xhttp.send();
 
 }
 
  function getLogout(){
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			logout();
 		}
 	}
 	
 	//xhttp.open("GET", "http://localhost:9989/employees/session");
 	
 	xhttp.send();
 
 }
 
 
 function logout(){
 	//console.log("In logout function");
 	localStorage.clear();
 }
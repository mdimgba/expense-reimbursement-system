/**
 * Getting a user session
 
   CLEAN THE CONSOLE LOGS BEFORE PRESENTING
 */
 //console.log("js is linked (login)");
 
 //localStorage.clear();
 
 // If I can't figure out how to pass the session between js files, I can
 // link all my html files to this js file and use comments to make it clearer
 // to me what code belongs to what page
 // resolved - localStorage
 
 window.onload = function(){
 	//localStorage.clear();
 	//console.log(" CLEAN BEFORE PRESENTING");
 
 	//document.getElementById('login-form').addEventListener('submit', postLoginEmployee);
 	////document.getElementById('login-form').addEventListener('submit', getSessionEmployee);
 	//document.getElementById('loginSubmit').addEventListener('click', getSessionEmployee);
 	
// 	document.getElementById('loginSubmit').addEventListener('click', postLoginEmployee);
 	document.getElementById('loginSubmit').addEventListener('click', getSessionEmployee);
 	
 	//getSessionEmployee();
 	//myStorage = window.localStorage;
 }
 
 function postLoginEmployee(){
 	let xhttp = new XMLHttpRequest();
 	
 	var data = new FormData();
 	var dataJSON = {
 	"uName": document.getElementById('username').value,
 	"pWord": document.getElementById('password').value
 	};
	data.append('username', document.getElementById('username').value);
	data.append('password', document.getElementById('password').value);
	var username = document.getElementById('username').value;
	var password = document.getElementById('password').value

 	xhttp.onreadystatechange = function(){
 	
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			uName = document.getElementById('username').value;
 			//console.log(uName);
 			data.username=uName;
 			//console.log(data.username);
 			pWord = document.getElementById('password').value;
 			//console.log(pWord);
 			data.password = pWord;
 			//console.log(data.password);
 		}
 	}
 	xhttp.open("POST", "http://localhost:9989/refund/login");
 	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
 	//xhttp.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
 	
 	xhttp.send(dataJSON);
 	
 	/*
 	//console.log(data.username);
 	//console.log(data.password);
 	//xhttp.send({"username": username, "password": password});
 	//xhttp.send(data);
 	//xhttp.send();
 	*/
 	
 }
 
 
 function getSessionEmployee(){
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			let emp = JSON.parse(xhttp.responseText);
 			//console.log(emp);
 			//This works
 			//console.log(emp.firstName);
 			//This doesn't work. Can't have obj of objects
 			//sessionStorage.setItem('emp', emp);
 			
 			//local storage global trial
 			localStorage.setItem('userId', emp.userId);
 			localStorage.setItem('userName', emp.userName);
 			localStorage.setItem('password', emp.pass);
 			localStorage.setItem('firstName', emp.firstName);
 			localStorage.setItem('lastName', emp.lastName);
 			localStorage.setItem('email', emp.email);
 			localStorage.setItem('isAdmin', emp.isAdmin);
 			
 			
 			/*
 			// Good ss
 			sessionStorage.setItem('userId', emp.userId);
 			sessionStorage.setItem('userName', emp.userName);
 			sessionStorage.setItem('password', emp.pass);
 			sessionStorage.setItem('firstName', emp.firstName);
 			sessionStorage.setItem('lastName', emp.lastName);
 			sessionStorage.setItem('email', emp.email);
 			sessionStorage.setItem('isAdmin', emp.isAdmin);
 			
 			// pass between files trial - works
 			myStorage.setItem('userId', emp.userId);
 			myStorage.setItem('userName', emp.userName);
 			myStorage.setItem('password', emp.pass);
 			myStorage.setItem('firstName', emp.firstName);
 			myStorage.setItem('lastName', emp.lastName);
 			myStorage.setItem('email', emp.email);
 			myStorage.setItem('isAdmin', emp.isAdmin);
 			*/
 			
 			
 			
 			
 			//Good ss
 			//let fName = sessionStorage.firstName;
 			//console.log(fName);
 			
 			// trial storage - works
 			//let fName = myStorage.firstName;
 			//console.log(fName);
 			
 			//local storage
 			let fName = localStorage.firstName;
 			//console.log(fName);
 			
 			//let emp2 = JSON.parse(sessionStorage.emp);
 			//console.log(emp2);
 		}
 	}
 	xhttp.open("GET", "http://localhost:9989/employees/session",false);
 	
 	xhttp.send();
 	
 }
 
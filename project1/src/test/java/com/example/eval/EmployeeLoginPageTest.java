package com.example.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.EmployeeLoginPage;

public class EmployeeLoginPageTest {
	
	private EmployeeLoginPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new EmployeeLoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testLoginHeader() {
		assertEquals(page.getHeader(), "Login Here");
	}
	
	@Test
	public void testSuccessfulLogin() {
		page.setUserName("JohnnyBoy");
		page.setPassword("pass");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/notadmin.html"));
		assertEquals("http://localhost:9989/html/notadmin.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testFailedLogin() {
		page.setUserName("Not");
		page.setPassword("Right");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/failedlogin.html"));
		assertEquals("http://localhost:9989/html/failedlogin.html", driver.getCurrentUrl());
	}

}

package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.RefundDBConnection;
import com.example.dao.RefundDao;
import com.example.models.Employee;
import com.example.models.Reimbursement;

// test dao methods that return single employee objects
public class RefundDaoEmpTest {

	private static String url = "jdbc:mariadb://database-1.cxmyzlwu53kp.us-east-2.rds.amazonaws.com:3306/refunddb";
	private static String username = "refunduser";
	private static String password = "mypassword";
	
	@Mock
	private RefundDBConnection rdbc;
	@Mock
	private Connection c;
	@Mock
	private PreparedStatement ps;
	@Mock
	private CallableStatement cs;
	@Mock
	private ResultSet rs1; /* (result set for employee) */
	@Mock
	private ResultSet rs2; /* result set for reimbursement */
	@Mock
	private Employee testEmployee;
	@Mock
	private Reimbursement testReimbursement;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		//c = rdbc.getDBConnection();
		when(rdbc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testEmployee = new Employee(1, "Johnny", "password", "Johnny", "Appleseed", "trees@yahoo.com", false);
		when(rs1.first()).thenReturn(true);
		when(rs1.getInt(1)).thenReturn(testEmployee.getUserId());
		when(rs1.getString(2)).thenReturn(testEmployee.getUserName());
		when(rs1.getString(3)).thenReturn(testEmployee.getPassword());
		when(rs1.getString(4)).thenReturn(testEmployee.getFirstName());
		when(rs1.getString(5)).thenReturn(testEmployee.getLastName());
		when(rs1.getString(6)).thenReturn(testEmployee.getEmail());
		when(rs1.getBoolean(7)).thenReturn(testEmployee.getIsAdmin());
		
		
		//when(ps.setInt(1, any(Integer.class))).thenReturn(testEmployee.getUserId());
		
		when(ps.executeQuery()).thenReturn(rs1);
		
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	// Methods that return a single employee
	@Test
	public void testFindEmployeeById() {
		assertEquals(new RefundDao(rdbc).findEmployeeById(1).getUserName(), testEmployee.getUserName());
	}
	
	@Test
	public void testFindEmployeeByUsername() {
		assertEquals(new RefundDao(rdbc).findEmployeeByUsername("Johnny").getEmail(), testEmployee.getEmail());
	}
	
	
}

package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.RefundDBConnection;
import com.example.dao.RefundDao;
import com.example.models.Employee;
import com.example.models.Reimbursement;

// test dao methods that return lists of refund objects
public class RefundDaoRefListTest {
	
	private static String url = "jdbc:mariadb://database-1.cxmyzlwu53kp.us-east-2.rds.amazonaws.com:3306/refunddb";
	private static String username = "refunduser";
	private static String password = "mypassword";
	
	@Mock
	private RefundDBConnection rdbc;
	@Mock
	private Connection c;
	@Mock
	private PreparedStatement ps;
	@Mock
	private CallableStatement cs;
	@Mock
	private ResultSet rs1; /* (result set for employee) */
	@Mock
	private ResultSet rs2; /* result set for reimbursement */
	@Mock
	private Employee testEmployee;
	@Mock
	private Employee testEmployeeTwo;
	@Mock
	private Employee testAdmin;
	@Mock
	private Reimbursement testReimb;
	@Mock
	private Reimbursement testReimbTwo;
	@Mock
	private Reimbursement testReimbThree;
	@Mock
	private Reimbursement testReimbFour;
	@Mock
	private Blob blob;
	@Mock
	private Timestamp timestamp;
	private static List<Reimbursement> tList;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		testEmployee = new Employee(1, "Johnny", "password", "Johnny", "Appleseed", "trees@yahoo.com", false);
		testEmployeeTwo = new Employee(2, "Static", "password", "Virgil", "Hawkins", "dakota@dc.com", false);
		testAdmin = new Employee(3, "NotBatman", "password", "Bruce", "Wayne", "bwayne@wayne.com", true);
		
//		Date date = new Date();
//		Timestamp time = new Timestamp(date.getTime());
		testReimb = new Reimbursement(1, 108.49, timestamp, timestamp, "Power lunch with coworkers", blob, 1, 3, 1, 2);
		testReimbTwo = new Reimbursement(2, 10000, timestamp, timestamp, "Red Octobers", blob, 1, 3, 3, 4);
		testReimbThree = new Reimbursement(3, 200.12, timestamp, timestamp, "Subway Pass", blob, 2, 3, 1, 3);
		testReimbFour = new Reimbursement(4, 160, timestamp, timestamp, "Weekend at Hotel", blob, 1, 3, 1, 1);

		List<Reimbursement> testList = new ArrayList<>();
		testList.add(testReimb);
		testList.add(testReimbTwo);
		testList.add(testReimbFour);
		
		List<Reimbursement> testListPending = new ArrayList<>();
		testListPending.add(testReimb);
		testListPending.add(testReimbFour);
		
		List<Reimbursement> testListAll = new ArrayList<>();
		testList.add(testReimb);
		testList.add(testReimbTwo);
		testList.add(testReimbThree);
		testList.add(testReimbFour);
		
		// handling FindAllReimbursementsById()
		when(rdbc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		
		when(ps.executeQuery()).thenReturn(rs1);
		
		int i=0;
		while(i<3) {
		when(rs1.next()).thenReturn(false);
		tList = testList;
		// when(tList.add(testReimb)).thenReturn(testList.get(i));
		// when(return tList).thenReturn(testList);
		
		/*
				when(rs1.getInt(1)).thenReturn(testReimbThree.getReimbId());
				when(rs1.getDouble(2)).thenReturn(testReimbThree.getReimbAmount());
				when(rs1.getTimestamp(3)).thenReturn(testReimbThree.getSubTime());
				when(rs1.getTimestamp(4)).thenReturn(testReimbThree.getResolvedTime());
				when(rs1.getString(5)).thenReturn(testReimbThree.getDescription());
				when(rs1.getBlob(6)).thenReturn(testReimbThree.getReceipt());
				when(rs1.getInt(7)).thenReturn(testReimbThree.getReimbAuthor());
				when(rs1.getInt(8)).thenReturn(testReimbThree.getReimbResolver());
				when(rs1.getInt(9)).thenReturn(testReimbThree.getReimbStatusId());
				when(rs1.getInt(10)).thenReturn(testReimbThree.getReimbTypeId());
				*/
		i++;
		}
		//tList = testList;
		when(tList).thenReturn(testList);
		
		//when(ps.setInt(1, any(Integer.class))).thenReturn(testEmployee.getUserId());
		
		
		
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	//@Test
	public void testFindReimbursementsById() {
		List<Reimbursement> testList = new ArrayList<>();
		testList.add(testReimb);
		testList.add(testReimbTwo);
		testList.add(testReimbFour);
		assertEquals(new RefundDao(rdbc).findReimbursementsById(1), testList);
		
	}
	
	//@Test
	public void testFindAllPendingReimbursements() {
		
	}

}

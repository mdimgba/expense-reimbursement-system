package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.RefundDao;
import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.models.ReimbursementStatus;
import com.example.service.RefundServiceAdmin;

public class RefundServiceAdminTest {
	
	@Mock
	private RefundDao mockedDao;
	private RefundServiceAdmin testService = new RefundServiceAdmin(mockedDao);
	private Employee testEmployee;
	private Employee testEmployeeTwo;
	private Employee testAdmin;
	private Reimbursement testReimb;
	private Reimbursement testReimbTwo;
	private Reimbursement testReimbThree;
	private Reimbursement testReimbFour;
	private ReimbursementStatus testStatus;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		
	}
	
	@AfterClass
	public static void setUpAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new RefundServiceAdmin(mockedDao);
		testEmployee = new Employee(1, "Johnny", "password", "Johnny", "Appleseed", "trees@yahoo.com", false);
		testEmployeeTwo = new Employee(2, "Virgil", "password", "Hawkins", "Static", "dakota@dc.com", false);
		testAdmin = new Employee(3, "Bruce", "password", "NotBatman", "Wayne", "bwayne@wayne.com", true);
		
		List<Reimbursement> testList = new ArrayList<>();
		testReimb = new Reimbursement(108.49, "Power lunch with coworkers", 1, 1, 2);
		testList.add(testReimb);
		testReimbTwo = new Reimbursement(10000, "Red Octobers", 1, 3, 4);
		testList.add(testReimbTwo);
		testReimbThree = new Reimbursement(200.12, "Subway Pass", 2, 1, 3);
		List<Reimbursement> testListTwo = new ArrayList<>();
		testListTwo.add(testReimbThree);
		
		List<Reimbursement> testListPending = new ArrayList<>();
		testListPending.add(testReimb);
		testListPending.add(testReimbThree);
		
		List<Reimbursement> testListAll = new ArrayList<>();
		testListAll.add(testReimb);
		testListAll.add(testReimbTwo);
		testListAll.add(testReimbThree);
		
		List<Reimbursement> testThree = new ArrayList<>();
		testThree.add(testReimbTwo);
		
		testStatus = new ReimbursementStatus(3);
		
		// mocking database for Pending Reimbursements
		when(mockedDao.findAllPendingReimbursements()).thenReturn(testListPending);
		
		// mocking database for Filter Reimbursements by Status
		when(mockedDao.findAllReimbursements()).thenReturn(testThree);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	// The function won't have access to my created employees and their
	// reimbursements b/c there is no database. I have to decipher that
	@Test
	public void testViewAllPendingReimbursementsSuccess() {
		List<Reimbursement> tTestListPending = new ArrayList<>();
		tTestListPending.add(testReimb);
		tTestListPending.add(testReimbThree);
		assertEquals(testService.viewAllPendingReimbursements(testAdmin), tTestListPending);
	}
	
	@Test(expected = NullPointerException.class)
	public void testViewAllPendingReimbursementsFailure() {
		assertEquals(testService.viewAllPendingReimbursements(testEmployeeTwo), null);
	}
	
	//The decide method here and the general analog are critical
	// functions. Maybe I can verify they work with dao tests
	//@Test
	public void testDecideReimbursementSuccess() {
		
	}
	
	//@Test
	public void testDecideReimbursementFailure() {
		
	}
	
	@Test
	public void testFilterReimbursementByStatusIdSuccess() {
		List<Reimbursement> tTestThree = new ArrayList<>();
		tTestThree.add(testReimbTwo);
		assertEquals(testService.filterReimbursementsByStatus(testAdmin, testStatus), tTestThree);
	}
	
	@Test(expected = NullPointerException.class)
	public void testFilterReimbursementByStatusIdFailure() {
		assertEquals(testService.viewAllPendingReimbursements(testEmployeeTwo), null);
	}
	
	

}

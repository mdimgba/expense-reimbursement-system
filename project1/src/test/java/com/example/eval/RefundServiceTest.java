package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.RefundDao;
import com.example.models.Employee;
import com.example.models.Reimbursement;
import com.example.service.RefundService;

public class RefundServiceTest {

	@Mock
	private RefundDao mockedDao;
	private RefundService testService = new RefundService(mockedDao);
	private Employee testEmployee;
	private Reimbursement testReimb;
	private Employee testEmployeeTwo;
	private Reimbursement testReimbTwo;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new RefundService(mockedDao);
		testEmployee = new Employee(1, "Johnny", "password", "Johnny", "Appleseed", "trees@yahoo.com", false);
		// When for testing verifyLoginCredentials
		when(mockedDao.findEmployeeByUsername("Johnny")).thenReturn(testEmployee);
		
		testReimb = new Reimbursement(108.49, "Power lunch with coworkers", 1, 1, 2);
		List<Reimbursement> testList = new ArrayList<>();
		testList.add(testReimb);
		testEmployeeTwo = new Employee(2, "Bruce", "password", "NotBatman", "Wayne", "BWayne@wayne.com", false);
		// When for testVerifyLoginCredentials
		when(mockedDao.findReimbursementsById(testEmployee.getUserId())).thenReturn(testList);
		when(mockedDao.findReimbursementsById(testEmployeeTwo.getUserId())).thenReturn(null);
		
		testReimbTwo = new Reimbursement(10000, "Red Octobers", 1, 3, 4);
		testList.add(testReimbTwo);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testVerifyLoginCredentialsSuccess() {
		assertTrue(testService.verifyLoginCredentials("Johnny", "password"));
	}
	
	@Test
	public void testVerifyLoginCredentialsFailure() {
		assertFalse(testService.verifyLoginCredentials("Johnny", "nope"));
	}
	
	@Test
	public void testRetrieveReimbursementsForEmployeeSuccess() {
		List<Reimbursement> tTestList = new ArrayList<>();
		tTestList.add(testReimb);
		tTestList.add(testReimbTwo);
		assertEquals(testService.retrieveReimbursemtsForEmployee(testEmployee), tTestList);
	}
	
	// I don't like this test very much. It feels like i'm feeding
	//the answer in directly
	// Negative test, needs work. What do I expect if its nothing?
	@Test(expected = NullPointerException.class)
	public void testRetrieveReimbursementsForEmployeeFailure() {
		List<Reimbursement> tTestList = new ArrayList<>();
		//testList.add(testReimb);
		assertEquals(testService.retrieveReimbursemtsForEmployee(testEmployeeTwo), null);
	}
	
	// I should organize all these into more compact test packages
	@Test
	public void testViewPastTicketsSuccess() {
		List<Reimbursement> tTestList = new ArrayList<>();
		tTestList.add(testReimbTwo);
		assertEquals(testService.viewPastTickets(testEmployee), tTestList);
	}
	
	@Test(expected = NullPointerException.class)
	public void testViewPastTicketsFailure() {
		assertEquals(testService.viewPastTickets(testEmployeeTwo), null);
	}
	
	@Test
	public void testViewPendingTicketsSuccess() {
		List<Reimbursement> tTestList = new ArrayList<>();
		tTestList.add(testReimb);
		assertEquals(testService.viewPendingTickets(testEmployee), tTestList);
	}
	
	@Test(expected = NullPointerException.class)
	public void testViewPendingTicketsFailure() {
		assertEquals(testService.viewPendingTickets(testEmployeeTwo), null);
	}
	
	@Test
	public void testAddNewTicketSuccess() {
		
	}
	
	@Test
	public void testAddNewTicketFailure() {
		
	}
	
	
	
	
}

/**
 * Admin Home page
 */
 
 window.onload = function(){
 	//console.log("js is linked");
 	
 	getSessionEmployee();
 	AdminDOMManip();
 
 	document.getElementById('logout').addEventListener("click", getLogout);
 }
 
 
function getSessionEmployee(){
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			let emp = JSON.parse(xhttp.responseText);
 			
 			//local storage global trial
 			localStorage.setItem('userId', emp.userId);
 			localStorage.setItem('userName', emp.userName);
 			localStorage.setItem('password', emp.pass);
 			localStorage.setItem('firstName', emp.firstName);
 			localStorage.setItem('lastName', emp.lastName);
 			localStorage.setItem('email', emp.email);
 			localStorage.setItem('isAdmin', emp.isAdmin);
 			
 		}
 	}
 	xhttp.open("GET", "http://localhost:9989/employees/session",false);
 	
 	xhttp.send();
 	
 }
 
 function AdminDOMManip(){
 	let fName = localStorage.firstName;
 	//console.log(fName);
 	let nameId = document.getElementById('adminNameplate').value;
 	//console.log(nameId);
 	document.getElementById('adminNameplate').innerHTML = "Welcome, " + localStorage.userName
 }
 
   function getLogout(){
 	let xhttp = new XMLHttpRequest();
 	
 	xhttp.onreadystatechange = function(){
 	
 		if(xhttp.readyState == 4 && xhttp.status == 200){
 			logout();
 		}
 	}
 	
 	xhttp.open("GET", "http://localhost:9989/employees/end");
 	
 	xhttp.send();
 
 }
 
 function logout(){
 	//console.log("In logout function");
 	localStorage.clear();
 }